<?php
declare(strict_types=1);
namespace App\Repositories;

interface PersistenceUnit
{
    public function generateId();

    public function persist(array $data);

    public function retrieve(int $id);

    public function delete(int $id);

    public function all();
}
