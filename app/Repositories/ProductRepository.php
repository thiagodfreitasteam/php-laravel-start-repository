<?php 

namespace App\Repositories;

use App\Product;
// use App\Repositories\PersistenceUnit;

class ProductRepository
{
    
    protected $persistence;

    // Constructor
    public function __construct(PersistenceUnit $persistence)
    {
        $this->persistence = $persistence;
    }

    // Get all instances
    public function all()
    {
        $array = $this->persistence->all();
        $retorno = [];
        $i = 0;
        if (isset($array) && count($array) > 0) {
            foreach ($array as $item) {
                $retorno[$i++] = new Product($item['id'], $item['nome']);
    
            }
        }
        return $retorno;
    }

    // create a new record in the database
    public function create(Product $produto)
    {
        $id = $this->persistence->generateId(); //Gera o id
        
        $this->persistence->persist([
            'id' => $id,
            'nome' => $produto->nome,
        ]);

    }

    // update record in the database
    public function update(Product $produto)
    {
        $record = $this->persistence->retrieve($produto->id);
        
        if ($record != null) {
            $this->persistence::persist([
                'id' => $produto->id,
                'nome' => $produto->nome,
            ]);
        }

    }

    // remove record from the database
    public function delete($id)
    {
        $record = $this->persistence::retrieve($id);
        if ($record != null) {
            $this->persistence->delete([
                'id' => $produto->id,
                'nome' => $produto->nome,
            ]);
        }
    }

    // show the record with the given id
    public function show($id)
    {
        $record = $this->persistence->retrieve($id);
        return new Product($record['id'], $record['nome']);
    }

}
