<?php
declare(strict_types=1);
namespace App\Repositories;

class InMemoryPersistenceUnit implements PersistenceUnit
{
    /**
     * @var array
     */
    private $data = [];

    public $id;

    function __construct() {
        $data = CacheManager::cache_get("data");
        if ($data === false) {
            $data = [];
            CacheManager::cache_set("data", $data);
            error_log("construindo persistence unit");
        }
        $this->data = $data;
    }

    /**
     * @var int
     */
    private $lastId = 0;

    public function generateId()
    {
        $retorno =count($this->data);
        $retorno++;
        $this->lastId = $retorno;
        return $this->lastId;
    }

    public function persist(array $data)
    {
        $this->data[$this->lastId] = $data;
        CacheManager::cache_set("data", $this->data);
    }

    public function retrieve(int $id)
    {
        if (!isset($this->data[$id])) {
            throw new \OutOfBoundsException(sprintf('No data found for ID %d', $id));
        }

        return $this->data[$id];
    }

    public function all()
    {
        return $this->data;
    }

    public function delete(int $id)
    {
        print "delete\n";
    }
}
