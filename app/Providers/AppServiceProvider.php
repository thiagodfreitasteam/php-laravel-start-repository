<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\InMemoryPersistenceUnit;
use App\Repositories\PersistenceUnit;
use App;


class AppServiceProvider extends ServiceProvider
{

    // public $singletons = [
    //     PersistenceUnit::class => InMemoryPersistenceUnit::class
    // ];


    public function register()
    {
        App::singleton(PersistenceUnit::class, function ($app) {
            $retorno = new InMemoryPersistenceUnit();
            return $retorno;
        });
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
