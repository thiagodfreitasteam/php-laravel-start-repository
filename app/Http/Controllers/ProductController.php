<?php

namespace App\Http\Controllers;

use App\Product;
use App\Repositories\ProductRepository;
use App\Repositories\PersistenceUnit;
// use App\Repositories\InMemoryPersistenceUnit;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    protected $produtos;

    function __construct(PersistenceUnit $unit) {
        $this->produtos = new ProductRepository($unit);
        // $this->produtos->create(new Product(0, 'Teste'));
        // $this->produtos->create(new Product(0, 'Teste1'));
    }
 
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->produtos->all();
        $comp = compact('products');
        return view('products.index', $comp)->with('i', 0);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->produtos->create(new Product(0, 'Teste'));
        $products = $this->produtos->all();
        $comp = compact('products');
        return view('products.index', $comp)->with('i', 0);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
