<?php
declare(strict_types=1);

namespace DesignPatterns\More\Repository\Tests;

use App\Product;
use App\Repositories\ProductRepository;
use App\Repositories\PersistenceUnit;
use App\Repositories\InMemoryPersistenceUnit;
use PHPUnit\Framework\TestCase;

class PostRepositoryTest extends TestCase
{
    /**
     * @var PostRepository
     */
    private $repository;

    protected function setUp(): void
    {
        $this->repository = new ProductRepository(new InMemoryPersistenceUnit());
    }

    public function testCanGenerateId()
    {
        $this->assertEquals(1, $this->repository->generateId()->toInt());
    }

}
